#coding:utf-8
from flask import Flask, request, abort
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError,LineBotApiError,
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage, Error, ImageSendMessage,
)
from linebot.__about__ import __version__

from linebot.http_client import HttpClient, RequestsHttpClient
import os,sys
import json

app = Flask(__name__)

line_bot_api = LineBotApi(os.environ["CHANNEL_ACCESS_TOKEN"])
handler = WebhookHandler(os.environ["CHANNEL_SECRET"])

@app.route('/')
def connect_test():
    return "access success!"

@app.route("/callback",methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']
    body = request.get_data(as_text=True)
    app.logger.info("Request body: "+ body)
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'

# messageが送られてきたら...
@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    reply_token = event.reply_token
    giphy_url = get_giphy_url(event.message.text)
    messages = TextSendMessage(text=f"OK,This is {event.message.text}.")
    picture = ImageSendMessage(
        original_content_url=giphy_url,
        preview_image_url=giphy_url
    )
    line_bot_api.reply_message(
        event.reply_token,
        [messages,picture])

''' giphy_client side '''
def get_giphy_url(keyword):
    return 'https://media.giphy.com/media/oyVGsLT6Kr38sUaeF9/giphy.gif'

if __name__ == "__main__":
    port = int(os.getenv("PORT",5000))
    app.run(debug=True,host="0.0.0.0",port=port)
   
